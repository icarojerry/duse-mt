This repository aim to implement a new feature for architectural metrics in DuSE-MT.
This feature is implemented through plugin JsArchitecturalMetrics.
The plugin JsArchitecturalMetrics contains the scripts of architectural metrics in javascript language.